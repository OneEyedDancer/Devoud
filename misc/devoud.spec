# -*- mode: python ; coding: utf-8 -*-


block_cipher = None


a = Analysis(
    ['../devoud/app.py'],
    pathex=[],
    binaries=[],
    datas=[('../devoud/media', 'media'), ('../devoud/browser/page/web/adblocker/rules', '/browser/page/web/adblocker/rules')],
    hiddenimports=[],
    hookspath=[],
    hooksconfig={},
    runtime_hooks=[],
    excludes=[],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher,
    noarchive=False,
)
pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)

exe = EXE(
    pyz,
    a.scripts,
    a.binaries,
    a.zipfiles,
    a.datas,
    [],
    name='devoud',
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    upx_exclude=[],
    runtime_tmpdir=None,
    console=False,
    disable_windowed_traceback=False,
    argv_emulation=False,
    target_arch=None,
    icon='../devoud/media/icons/devoud.ico',
    codesign_identity=None,
    entitlements_file=None,
)
