search_engines = {
    'Yandex': {
        'site': 'https://ya.ru/',
        'query': 'https://yandex.ru/search/?text=',
        'icon': 'icons:yandex.svg',
    },
    'Google': {
        'site': 'https://www.google.com/',
        'query': 'https://www.google.com/search?q=',
        'icon': 'icons:google.svg',
    },
    'Bing': {
        'site': 'https://www.bing.com/',
        'query': 'https://www.bing.com/search?q=',
        'icon': 'icons:bing.svg',
    },
    'DDGo': {
        'site': 'https://duckduckgo.com/',
        'query': 'https://duckduckgo.com/?q=',
        'icon': 'icons:duckduckgo.svg',
    },
    'Wiki': {
        'site': 'https://wikipedia.org/',
        'query': 'https://wikipedia.org/w/index.php?search=&title=',
        'icon': 'icons:wiki.svg',
    },
    'You': {
        'site': 'https://you.com/',
        'query': 'https://you.com/search?q=',
        'icon': 'icons:you.png',
    },
    'StartPage': {
        'site': 'https://www.startpage.com/',
        'query': 'https://www.startpage.com/sp/search?query=',
        'icon': 'icons:startpage.svg',
    },
    'Shodan': {
        'site': 'https://www.shodan.io/',
        'query': 'https://www.shodan.io/search?query=',
        'icon': 'icons:shodan.png',
    }
}
